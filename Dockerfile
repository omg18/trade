#Build from base image is java 11
FROM openjdk:11

# Get the path of file .jar in local directory
ARG JAR_FILE=target/trade*.jar

# Create a working dir in docker
WORKDIR /opt/app

# Set volume point to /tmp
VOLUME /tmp

# Copy file .jar from local to the working dir in docker
# and rename file is trade.jar
COPY ${JAR_FILE} trade.jar

# define the command default. After container start, these commands are executed.
ENTRYPOINT ["java","-jar","trade.jar"]

EXPOSE 10273

package com.omg.trade.message.consumer;


import com.omg.trade.constant.OrderStatus;
import com.omg.trade.entity.Orders;
import com.omg.trade.message.BaseConsumerService;
import com.omg.trade.model.dto.ConfirmPaymentDTO;
import com.omg.trade.repository.OrderRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author nhcuong
 */
@Service
@Slf4j
public class PaymentConfirmConsumer implements BaseConsumerService<ConfirmPaymentDTO> {

    @Autowired
    private OrderRepository orderRepository;

    @KafkaListener(
            topics = "${spring.kafka.topic.confirm-payment}"
            ,groupId = "${spring.kafka.consumer.group-id}"
    )
    @Override
    public void receive(ConfirmPaymentDTO data) {

        // Check message data
        if (data == null) {
            log.info(" The message is null");
            return;
        }

        // Check order info
        Optional<Orders> order = this.orderRepository.getOrderByCode(data.getCustomerId(), data.getOrderCode());
        if (order.isEmpty()) {
            log.info(" The order info does not exist.");
            return;
        }

        // Check status of order
        Orders orderData = order.get();
        if (OrderStatus.CREATE.getValue().equals(orderData.getStatus())
                || OrderStatus.PAID.getValue().equals(orderData.getStatus())) {
            log.info(" The order status is invalid.");
            return;
        }

        // Update status of order
        orderData.setStatus(OrderStatus.PAID.getValue());
        this.orderRepository.save(orderData);
    }
}

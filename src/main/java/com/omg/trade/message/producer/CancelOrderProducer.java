package com.omg.trade.message.producer;

import com.omg.trade.message.BaseProducerService;
import com.omg.trade.model.dto.CancelOrderDTO;
import com.omg.trade.model.dto.ConfirmOrderDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

/**
 * @author nhcuong
 */
@Service
@Slf4j
public class CancelOrderProducer extends BaseProducerService<CancelOrderDTO> {

    @Value("${spring.kafka.topic.cancel-order}")
    private String cancelTopicName;

    @Autowired
    public CancelOrderProducer(KafkaTemplate<String, CancelOrderDTO> kafkaTemplate) {
        super(kafkaTemplate);
    }

    @Override
    protected String getTopicName() {
        return this.cancelTopicName;
    }
}

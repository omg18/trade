package com.omg.trade.message.producer;

import com.omg.trade.message.BaseProducerService;
import com.omg.trade.model.dto.ConfirmOrderDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

/**
 * @author nhcuong
 */
@Service
@Slf4j
public class ConfirmOrderProducer extends BaseProducerService<ConfirmOrderDTO> {

    @Value(value = "${spring.kafka.topic.confirm-order}")
    private String topicName;

    @Autowired
    public ConfirmOrderProducer(KafkaTemplate<String, ConfirmOrderDTO> kafkaTemplate) {
        super(kafkaTemplate);
    }

    @Override
    protected String getTopicName() {
        return this.topicName;
    }

}

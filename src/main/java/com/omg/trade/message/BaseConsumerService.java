package com.omg.trade.message;

public interface BaseConsumerService<T>{

    void receive(T data);
}

package com.omg.trade.constant;

/**
 * @author hieudn
 * This class defines all enum of UUID prefix.
 */
public enum UUIDPrefixEnum {

    ORDER("ORDER_"),
    ORDER_BILL("ORDER_B_"),
    BILL("BILL_"),
    ORDER_DETAIL("ORDER_D_"),
    CART("CART_"),
    CART_ITEM("CART_I"),
    PLAN("PLAN_");

    private final String value;

    UUIDPrefixEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}

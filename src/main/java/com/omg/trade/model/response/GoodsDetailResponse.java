package com.omg.trade.model.response;

import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author nhcuong
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GoodsDetailResponse implements Serializable {

    private String code;

    private String name;

    private String info;

    private String categoryCode;

    private String categoryName;

    private String vendorCode;

    private String vendorName;

    private String planId;

    private BigDecimal unitPrice;

    private Long totalAmount;

    private Long confirmedQuantity;

    private Long soldQuantity;

    private LocalDateTime effectiveFromDate;

    private LocalDateTime effectiveToDate;
}

package com.omg.trade.model.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 *  @author cuongnh
 *  Product page request
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GoodsPageRequest {

    private Integer pageIndex;

    private Integer pageSize;

    private Long customerId;
}

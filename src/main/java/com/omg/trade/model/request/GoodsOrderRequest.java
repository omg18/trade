package com.omg.trade.model.request;

import lombok.*;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GoodsOrderRequest {

    private Long quantity;

    private String planId;
}

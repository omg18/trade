package com.omg.trade.model.request;

import lombok.*;

import javax.validation.constraints.NotNull;
import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RemoveGoodsRequest {

    private List<String> planIds;

    @NotNull
    private Long customerId;
}

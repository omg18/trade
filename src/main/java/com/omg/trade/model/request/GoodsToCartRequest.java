package com.omg.trade.model.request;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GoodsToCartRequest {

    @NotBlank
    private String planId;

    @NotNull
    private Long customerId;

    @NotNull
    private Long quantity;

    private String organizationCode;
}

package com.omg.trade.model.request;


import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CancelOrderRequest {

    @NotNull
    private Long customerId;

    @NotBlank
    private String orderCode;
}

package com.omg.trade.model.request;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.omg.trade.constant.AppConstant;
import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PlanRequest {

    private String planId;

    @NotBlank
    private String vendorCode;

    @NotBlank
    private String name;

    @NotBlank
    private String productCode;

    @NotNull
    private BigDecimal unitPrice;

    @NotNull
    private Long totalAmount;

    @NotNull
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = AppConstant.LOCAL_DATETIME_DEFAULT_FORMAT)
    private LocalDateTime effectiveFromDate;

    @NotNull
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = AppConstant.LOCAL_DATETIME_DEFAULT_FORMAT)
    private LocalDateTime effectiveToDate;
}

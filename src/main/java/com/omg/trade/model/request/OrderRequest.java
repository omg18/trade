package com.omg.trade.model.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderRequest {

    @NotNull
    private Long customerId;

    private List<GoodsOrderRequest> goods;

    @NotNull
    private LocalDateTime orderDate;

    private String description;

    @NotNull
    private BigDecimal totalPrice;

    @NotNull
    private Boolean includedTax;

    @NotNull
    private BigDecimal tax;
}

package com.omg.trade.controller;

import com.omg.trade.exception.TradeBusinessException;
import com.omg.trade.model.request.GoodsPageRequest;
import com.omg.trade.model.request.GoodsToCartRequest;
import com.omg.trade.model.request.RemoveGoodsRequest;
import com.omg.trade.model.response.PageResultResponse;
import com.omg.trade.model.response.GoodsDetailResponse;
import com.omg.trade.service.ICartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author nhcuong
 *
 * Define endpoints to process cart's customer.
 */
@RestController
@RequestMapping("api/cart")
public class CartController {

    @Autowired
    private ICartService cartService;

    /**
     * The endpoint allow to get the item details
     * @param code
     * @return
     */
    @GetMapping("/goods/details/{code}")
    public GoodsDetailResponse getProductDetailsByCode(@PathVariable("code") @NotNull String code) throws TradeBusinessException {
        return this.cartService.getProductDetailsByCode(code);
    }

    /**
     * Add a goods to cart
     *
     * @param request
     * @throws TradeBusinessException
     */
    @PostMapping("/add-goods")
    public void addGoodsToCart(@RequestBody @Valid GoodsToCartRequest request) throws TradeBusinessException {
        this.cartService.addGoodsToCart(request);
    }

    /**
     * Remove a goods from customer's cart
     * @param request
     */
    @DeleteMapping("/remove-goods")
    public void removeGoodsFromCart(@RequestBody @Valid RemoveGoodsRequest request) throws TradeBusinessException {
        this.cartService.removeGoodsFromCart(request);
    }

    /**
     * Get Goods info in the customer's cart
     *
     * @param request
     * @return
     * @throws TradeBusinessException
     */
    @GetMapping("/goods")
    public PageResultResponse<GoodsDetailResponse> getGoodsOfCart(@RequestBody @Valid GoodsPageRequest request) throws TradeBusinessException {
       return this.cartService.getGoodsOfCart(request);
    }
}

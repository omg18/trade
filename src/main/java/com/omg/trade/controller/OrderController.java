package com.omg.trade.controller;

import com.omg.trade.exception.TradeBusinessException;
import com.omg.trade.model.request.CancelOrderRequest;
import com.omg.trade.model.request.ConfirmOrderRequest;
import com.omg.trade.model.request.OrderRequest;
import com.omg.trade.service.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author nhcuong
 *
 * Define endpoints to process order's customer.
 */
@RestController
@RequestMapping("api/order")
public class OrderController {

    @Autowired
    private IOrderService orderService;

    /**
     * This API allows customer can place an order
     *
     * @param request
     */
    @PostMapping("/place-order")
    public void placeAnOrder(@RequestBody @Valid OrderRequest request) throws TradeBusinessException {

        this.orderService.placeAndOrder(request);
    }

    @PutMapping("/confirm-order")
    public void confirmOrder(@RequestBody @Valid ConfirmOrderRequest request) throws TradeBusinessException {
        this.orderService.confirmOrder(request);
    }

    @PutMapping("/cancel-order")
    public void cancelOrder(@RequestBody @Valid CancelOrderRequest request) throws TradeBusinessException {
        this.orderService.cancelOrder(request);
    }
}

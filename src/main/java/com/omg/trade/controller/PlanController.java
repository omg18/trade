package com.omg.trade.controller;

import com.omg.trade.exception.TradeBusinessException;
import com.omg.trade.model.request.PlanRequest;
import com.omg.trade.service.IPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

/**
 * @author nhcuong
 *
 * The APIs of plan feature.
 */
@RestController
@RequestMapping("api/plan")
public class PlanController {

    @Autowired
    private IPlanService planService;

    /**
     * The API helps to create new plan
     *
     * @param request
     * @throws TradeBusinessException
     */
    @PostMapping("/create")
    public void createPlan(@RequestBody @Valid PlanRequest request) throws TradeBusinessException {
        this.planService.createPlan(request);
    }

    /**
     * The API helps to update a plan
     *
     * @param request
     */
    @PostMapping("/update")
    public void updatePlan(@RequestBody @Valid PlanRequest request) throws TradeBusinessException {
        this.planService.updatePlan(request);
    }

    /**
     * The API helps to terminate a plan
     *
     * @param planId
     */
    @PutMapping("/terminate/{planId}")
    public void terminatePlan(@PathVariable("planId") @NotBlank String planId) throws TradeBusinessException {
        this.planService.terminatePlan(planId);
    }
}

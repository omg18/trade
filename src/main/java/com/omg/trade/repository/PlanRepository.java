package com.omg.trade.repository;

import com.omg.trade.entity.Plan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author nhcuong
 */
@Repository
public interface PlanRepository extends JpaRepository<Plan, String> {

    @Query(value = "SELECT * FROM plan p WHERE p.id IN :planIds AND p.is_terminated != 1", nativeQuery = true)
    List<Plan> findByPlanIds(@Param("planIds") List<String> planIds);
}

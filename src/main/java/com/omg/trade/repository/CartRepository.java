package com.omg.trade.repository;

import com.omg.trade.entity.Cart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

/**
 * @author nhcuong
 */
public interface CartRepository extends JpaRepository<Cart, String> {

    @Query(value = "SELECT * FROM cart c WHERE c.customer_id = :customerId AND c.is_terminated != 1", nativeQuery = true)
    Optional<Cart> getCartByCustomerId(@Param("customerId") Long customerId);
}

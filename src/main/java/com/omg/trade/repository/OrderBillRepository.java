package com.omg.trade.repository;

import com.omg.trade.entity.OrderBill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderBillRepository extends JpaRepository<OrderBill, String> {

    @Query(value = "SELECT ob.bill_id FROM order_bill ob WHERE ob.order_id = :orderId AND ob.inactive != 1",nativeQuery = true)
    String getBillIdByOrderId(@Param("orderId") String orderId);
}

package com.omg.trade.repository;

import com.omg.trade.entity.CartItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author nhcuong
 */
@Repository
public interface CartItemRepository extends JpaRepository<CartItem, String> {

    @Query(value = "SELECT * " +
                    "FROM cart_item ci " +
                    "WHERE ci.plan_id IN :planIds " +
                    "AND ci.cart_id = :cartId",
            nativeQuery = true)
    List<CartItem> queryCartItems(@Param("planIds") List<String> planIds, @Param("cartId") String cartId);

    @Query(value = "SELECT ci.plan_id FROM cart_item ci WHERE ci.cart_id = :cartId", nativeQuery = true)
    List<String> getPlanIdsByCartId(@Param("cartId") String cartId);
}

package com.omg.trade.repository;

import com.omg.trade.entity.Orders;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface OrderRepository extends JpaRepository<Orders, String> {

    @Query(value = "SELECT * FROM orders o WHERE o.code = :code AND o.customer_id = :customerId AND o.inactive != 1", nativeQuery = true)
    Optional<Orders> getOrderByCode(@Param("customerId") Long customerId, @Param("code") String code);
}

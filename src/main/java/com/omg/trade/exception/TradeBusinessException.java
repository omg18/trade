package com.omg.trade.exception;

/**
 * @author cuongnh
 * The custom exception to handle the business exception
 */
public class TradeBusinessException extends Exception{

    public TradeBusinessException(String errorMessage){
        super(errorMessage);
    }
}

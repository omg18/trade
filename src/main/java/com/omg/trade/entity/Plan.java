package com.omg.trade.entity;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name = "plan")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Plan {

    @Id
    private String id;

    private String name;

    private String productCode;

    private String vendorCode;

    private BigDecimal unitPrice;

    private Long totalAmount;

    private Long confirmedQuantity;

    private Long soldQuantity;

    private LocalDateTime effectiveFromDate;

    private LocalDateTime effectiveToDate;

    private Boolean isTerminated;

    private String createdBy;

    private String updatedBy;

    @CreationTimestamp
    private LocalDateTime createdTime;

    @UpdateTimestamp
    private LocalDateTime updatedTime;
}

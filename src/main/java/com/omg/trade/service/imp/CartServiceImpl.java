package com.omg.trade.service.imp;

import com.omg.trade.constant.UUIDPrefixEnum;
import com.omg.trade.entity.Cart;
import com.omg.trade.entity.CartItem;
import com.omg.trade.entity.Plan;
import com.omg.trade.exception.TradeBusinessException;
import com.omg.trade.model.request.GoodsPageRequest;
import com.omg.trade.model.request.GoodsToCartRequest;
import com.omg.trade.model.request.RemoveGoodsRequest;
import com.omg.trade.model.response.GoodsDetailResponse;
import com.omg.trade.model.response.PageResultResponse;
import com.omg.trade.repository.CartItemRepository;
import com.omg.trade.repository.PlanRepository;
import com.omg.trade.repository.CartRepository;
import com.omg.trade.service.ICartService;
import com.omg.trade.service.IProductClientService;
import com.omg.trade.utils.AppUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author nhcuong
 *
 * Process the cart business.
 */
@Service
public class CartServiceImpl implements ICartService {

    @Autowired
    private IProductClientService productClientService;

    @Autowired
    private PlanRepository planRepository;

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private CartItemRepository cartItemRepository;

    /**
     * Get the item details.
     *
     * @param code
     * @return
     */
    @Override
    public GoodsDetailResponse getProductDetailsByCode(String code) throws TradeBusinessException {

        // Get item details from product module
        GoodsDetailResponse goodsDetailResponse = this.productClientService.getProductDetail(code);

        // get plan for product.
        List<Plan> productPlan = this.planRepository.findByPlanIds(new ArrayList<>(Arrays.asList(code)));
        if( CollectionUtils.isEmpty(productPlan)) {
            throw new TradeBusinessException("There is no any plan for product with code:" + code);
        }

        // Set plan info for response.
        this.fillPlanInfo(goodsDetailResponse, productPlan.get(0));

        return goodsDetailResponse;
    }

    /**
     * Add goods into the cart
     * @param request
     * @throws TradeBusinessException
     */
    @Override
    public void addGoodsToCart(GoodsToCartRequest request) throws TradeBusinessException {

        // Get product details from product module
        Optional<Plan> plan = this.planRepository.findById(request.getPlanId());
        if (plan.isEmpty()) {
            throw new TradeBusinessException("The plan is not exist with Id" + request.getPlanId());
        }

        // Check exist product detail in database
        String productCode = plan.get().getProductCode();
        GoodsDetailResponse goodsDetailResponse =
                this.productClientService.getProductDetail(productCode);
        if (goodsDetailResponse == null
                || StringUtils.isEmpty(goodsDetailResponse.getCode())) {
            throw new TradeBusinessException("The item is not exist with code" + productCode);
        }

        // Get the cart's customer
        Optional<Cart> customerCart = this.cartRepository.getCartByCustomerId(request.getCustomerId());
        if(customerCart.isEmpty()) {
            throw new TradeBusinessException("The cart of customer does not exist. Customer Id:" + request.getCustomerId());
        }

        // Save the cart info into database
        this.cartItemRepository.save(
            CartItem.builder()
                .id(AppUtils.generateUUID(UUIDPrefixEnum.CART_ITEM.getValue()))
                .cartId(customerCart.get().getId())
                .planId(request.getPlanId())
                .quantity(request.getQuantity())
                .build()
        );
    }

    /**
     * Remove Goods from cart
     * @param request
     * @throws TradeBusinessException
     */
    @Transactional
    @Override
    public void removeGoodsFromCart(RemoveGoodsRequest request) throws TradeBusinessException {

        // Check product codes list
        if (CollectionUtils.isEmpty(request.getPlanIds())) {
            throw new TradeBusinessException("The plan ids list is not empty.");
        }

        // Get the cart's customer
        Optional<Cart> customerCart = this.cartRepository.getCartByCustomerId(request.getCustomerId());
        if(customerCart.isEmpty()) {
            throw new TradeBusinessException("The cart of customer does not exist. Customer Id:" + request.getCustomerId());
        }

        // Get removed products in Cart
        List<CartItem> cartItems = this.cartItemRepository.queryCartItems(request.getPlanIds(), customerCart.get().getId());
        if (CollectionUtils.isEmpty(cartItems)) {
            return;
        }

        // Execute remove products from cart
        this.cartItemRepository.deleteAll(cartItems);
    }

    /**
     * Get Goods info in the customer's cart
     *
     * @param request
     * @return
     * @throws TradeBusinessException
     */
    @Override
    public PageResultResponse<GoodsDetailResponse> getGoodsOfCart(GoodsPageRequest request) throws TradeBusinessException {

        // Check customer ID
        if (request.getCustomerId() == null) {
            throw new TradeBusinessException("The customerId is not null ");
        }

        // Check page index and page size
        Integer pageIndex = request.getPageIndex();
        Integer pageSize = request.getPageSize();
        if (Optional.ofNullable(pageIndex).orElse(0) == 0
                || Optional.ofNullable(pageSize).orElse(0) == 0) {
            request.setPageIndex(1);
            request.setPageSize(10);
        }

        // Get the cart's customer
        Optional<Cart> customerCart = this.cartRepository.getCartByCustomerId(request.getCustomerId());
        if(customerCart.isEmpty()) {
            throw new TradeBusinessException("The cart of customer does not exist. Customer Id:" + request.getCustomerId());
        }

        // Get product code of Goods in the cart.
        PageResultResponse emptyResponse = new PageResultResponse();
        emptyResponse.setPageIndex(request.getPageIndex());
        emptyResponse.setPageSize(request.getPageSize());

        // Get goods (product code) of cart and check
        List<String> planIds = this.cartItemRepository.getPlanIdsByCartId(customerCart.get().getId());
        if (CollectionUtils.isEmpty(planIds)) {
            return emptyResponse;
        }

        // Get plan of goods(product) and check
        List<Plan> plans = this.planRepository.findByPlanIds(planIds);
        if (CollectionUtils.isEmpty(plans)) {
            return emptyResponse;
        }

        // Get detail info of goods(product) and check
        List<String> productCodes = plans.stream().map(Plan::getProductCode).collect(Collectors.toList());
        PageResultResponse<GoodsDetailResponse> pageResultResponse =
                this.productClientService.getProductPageByCodes(request.getPageIndex(), request.getPageSize(), productCodes);
        List<GoodsDetailResponse> goodsDetailResponses = pageResultResponse.getContent();
        if (CollectionUtils.isEmpty(goodsDetailResponses)) {
            return emptyResponse;
        }

        // Set plan info for response
        Map<String, Plan> planMap = plans.stream().collect(Collectors.toMap(Plan::getProductCode, Function.identity()));
        for (GoodsDetailResponse item : goodsDetailResponses) {
            Plan plan = planMap.get(item.getCode());
             if (plan == null) {
                 continue;
             }
           this.fillPlanInfo(item, plan);
        }

        return pageResultResponse;
    }

    /**
     * Fill plan info for goods response
     *
     * @param response
     * @param productPlanData
     */
    private void fillPlanInfo(GoodsDetailResponse response, Plan productPlanData) {
        response.setUnitPrice(productPlanData.getUnitPrice());
        response.setTotalAmount(productPlanData.getTotalAmount());
        response.setConfirmedQuantity(productPlanData.getConfirmedQuantity());
        response.setSoldQuantity(productPlanData.getSoldQuantity());
        response.setEffectiveFromDate(productPlanData.getEffectiveFromDate());
        response.setEffectiveToDate(productPlanData.getEffectiveToDate());
        response.setPlanId(productPlanData.getId());
    }

}

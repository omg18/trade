package com.omg.trade.service.imp;

import com.omg.trade.constant.AppConstant;
import com.omg.trade.constant.OrderStatus;
import com.omg.trade.constant.UUIDPrefixEnum;
import com.omg.trade.entity.*;
import com.omg.trade.exception.TradeBusinessException;
import com.omg.trade.message.producer.CancelOrderProducer;
import com.omg.trade.message.producer.ConfirmOrderProducer;
import com.omg.trade.model.dto.CancelOrderDTO;
import com.omg.trade.model.dto.ConfirmOrderDTO;
import com.omg.trade.model.request.CancelOrderRequest;
import com.omg.trade.model.request.ConfirmOrderRequest;
import com.omg.trade.model.request.GoodsOrderRequest;
import com.omg.trade.model.request.OrderRequest;
import com.omg.trade.model.response.GoodsDetailResponse;
import com.omg.trade.repository.*;
import com.omg.trade.service.IOrderService;
import com.omg.trade.service.IProductClientService;
import com.omg.trade.utils.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class OrderServiceImpl implements IOrderService {

    @Autowired
    private PlanRepository planRepository;

    @Autowired
    private OrderDetailRepository orderDetailRepository;

    @Autowired
    private BillRepository billRepository;

    @Autowired
    private OrderBillRepository orderBillRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private IProductClientService productClientService;

    @Autowired
    private ConfirmOrderProducer confirmOrderProducer;

    @Autowired
    private CancelOrderProducer cancelOrderProducer;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void placeAndOrder(OrderRequest request) throws TradeBusinessException {

        // Check parameter
        List<GoodsOrderRequest> goodsOrder = request.getGoods();
        if (CollectionUtils.isEmpty(goodsOrder)) {
            throw new TradeBusinessException("The product code list is not empty");
        }

        // Check plan in order
        List<String> planIds =
                goodsOrder.stream().map(GoodsOrderRequest::getPlanId).collect(Collectors.toList());
        List<Plan> plans = this.planRepository.findByPlanIds(planIds);
        if (CollectionUtils.isEmpty(plans)) {
            throw new TradeBusinessException("The plan of products do not exist.");
        }

        // Check product info in plan
        List<String> productCodes = plans.stream().map(Plan::getProductCode).collect(Collectors.toList());
        this.checkValidProductsByCodes(productCodes);

        // save info
        // Save order info
        String orderId = AppUtils.generateUUID(UUIDPrefixEnum.ORDER.getValue());
        this.orderRepository.save(
                Orders.builder()
                .id(orderId)
                .code(orderId)
                .orderDate(request.getOrderDate())
                .status(OrderStatus.CREATE.getValue())
                .customerId(request.getCustomerId())
                .description(request.getDescription() == null ? AppConstant._EMPTY : request.getDescription())
                .inactive(false)
                .build());

        // Save order detail info
        List<OrderDetail> orderDetails = new ArrayList<OrderDetail>();
        for (GoodsOrderRequest item : goodsOrder) {
            orderDetails.add(
                    OrderDetail.builder()
                            .id(AppUtils.generateUUID(UUIDPrefixEnum.ORDER_DETAIL.getValue()))
                            .orderId(orderId)
                            .planId(item.getPlanId())
                            .quantity(item.getQuantity())
                            .inactive(false)
                            .build()
            );
        }
        this.orderDetailRepository.saveAll(orderDetails);

        // Save Bill info
        String billId = AppUtils.generateUUID(UUIDPrefixEnum.BILL.getValue());
        this.billRepository.save(
                Bill.builder()
                        .id(billId)
                        .code(billId)
                        .totalPrice(request.getTotalPrice())
                        .tax(request.getTax())
                        .includeTax(request.getIncludedTax())
                        .inactive(false)
                        .build()
        );

        // Save order bill info
        String orderBillId = AppUtils.generateUUID(UUIDPrefixEnum.ORDER_BILL.getValue());
        this.orderBillRepository.save(
                OrderBill.builder()
                        .id(orderBillId)
                        .orderId(orderId)
                        .billId(billId)
                        .inactive(false)
                        .build()
        );
    }

    /**
     * The vendor confirm order of customer
     *
     * @param request
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void confirmOrder(ConfirmOrderRequest request) throws TradeBusinessException {

        // Get order and check exist in data base
        Optional<Orders> order = this.orderRepository.getOrderByCode(request.getCustomerId(), request.getOrderCode());
        if (order.isEmpty()) {
           throw new TradeBusinessException("Do not exist this order with code " + request.getOrderCode());
        }
        Orders ordersData = order.get();

        // Check order status
        if (!OrderStatus.CREATE.getValue().equals(ordersData.getStatus())) {
            throw new TradeBusinessException("The order status is invalid ");
        }

        // Get Order detail
        List<OrderDetail> orderDetails = this.orderDetailRepository.findAllByOrderId(ordersData.getId());

        // Get plan with quantity
        Map<String, Long> planQuantityMap =
                orderDetails.stream().collect(Collectors.toMap(OrderDetail::getPlanId, OrderDetail::getQuantity));

        // Get plan ids
        List<String> planIds = orderDetails.stream().map(OrderDetail::getPlanId).collect(Collectors.toList());

        List<Plan> plans = this.planRepository.findByPlanIds(planIds);
        for (Plan plan : plans) {
            plan.setConfirmedQuantity(plan.getConfirmedQuantity() + planQuantityMap.get(plan.getId()));
        }

        // Update order status and plan info
        ordersData.setStatus(OrderStatus.CONFIRM.getValue());
        this.orderRepository.save(ordersData);
        this.planRepository.saveAll(plans);

        // Send payment info to Payment service (using kafka)
        String billId = this.orderBillRepository.getBillIdByOrderId(ordersData.getId());
        this.confirmOrderProducer.send(ConfirmOrderDTO.builder()
                        .customerId(request.getCustomerId())
                        .status(OrderStatus.CONFIRM.getValue())
                        .orderCode(ordersData.getCode())
                        .billCode( billId)
                        .channelPayment(request.getChannelPayment())
                        .build());
    }

    @Override
    public void cancelOrder(CancelOrderRequest request) throws TradeBusinessException {
        // Get order and check exist in data base
        Optional<Orders> order = this.orderRepository.getOrderByCode(request.getCustomerId(), request.getOrderCode());
        if (order.isEmpty()) {
            throw new TradeBusinessException("Do not exist this order with code " + request.getOrderCode());
        }
        Orders ordersData = order.get();

        // Check order status
        if (OrderStatus.PAID.getValue().equals(ordersData.getStatus())
                || OrderStatus.CANCEL.getValue().equals(ordersData.getStatus())) {
            throw new TradeBusinessException("Cannot cancel order with status is paid or cancel");
        }

        // execute cancel order
        ordersData.setStatus(OrderStatus.CANCEL.getValue());
        this.orderRepository.save(ordersData);

        String billId = this.orderBillRepository.getBillIdByOrderId(ordersData.getId());
        Bill bill = this.billRepository.getReferenceById(billId);

        // Send message to payment service to update payment info
        this.cancelOrderProducer.send(CancelOrderDTO.builder()
                    .customerId(request.getCustomerId())
                    .billCode(bill.getCode())
                    .orderCode(ordersData.getCode())
                    .status(OrderStatus.CANCEL.getValue())
                    .build());
    }

    /**
     * Check valid the product info in the plans
     * @param productCodes
     * @return
     * @throws TradeBusinessException
     */
    private List<GoodsDetailResponse> checkValidProductsByCodes(List<String> productCodes) throws TradeBusinessException {

        // Get product info from Product service
        List<GoodsDetailResponse> productsRes =
               this.productClientService.getProducstByCodes(productCodes);

        // Check products list is empty or not
        if (CollectionUtils.isEmpty(productsRes)) {
            throw new TradeBusinessException("The products do not exist");
        }

        // Check product exists in system or not
        List<String> productCodesRes =
                productsRes.stream().map(GoodsDetailResponse::getCode).collect(Collectors.toList());

        List<String> invalidProductCodes = new ArrayList<String>();
        for (String productCode : productCodes) {
            if (!productCodesRes.contains(productCode)) {
                invalidProductCodes.add(productCode);
            }
        }

        if (!CollectionUtils.isEmpty(invalidProductCodes)) {
            throw new TradeBusinessException("The products do not exist with codes: " + String.join(",", invalidProductCodes));
        }

        return productsRes;
    }
}

package com.omg.trade.service.imp;

import com.omg.trade.constant.UUIDPrefixEnum;
import com.omg.trade.entity.Plan;
import com.omg.trade.exception.TradeBusinessException;
import com.omg.trade.model.request.PlanRequest;
import com.omg.trade.model.response.GoodsDetailResponse;
import com.omg.trade.repository.PlanRepository;
import com.omg.trade.service.IPlanService;
import com.omg.trade.service.IProductClientService;
import com.omg.trade.utils.AppUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author nhcuong
 *
 * This service layer handle all business of plan
 */
@Service
public class PlanServiceImpl implements IPlanService {

    @Autowired
    private PlanRepository planRepository;

    @Autowired
    private IProductClientService productClientService;

    /**
     * Create new plan for Goods/product
     *
     * @param request
     * @throws TradeBusinessException
     */
    @Override
    public void createPlan(PlanRequest request) throws TradeBusinessException {

        // Get item details from product module
        GoodsDetailResponse goodsDetailResponse = this.productClientService.getProductDetail(request.getProductCode());
        if (goodsDetailResponse == null || StringUtils.isEmpty(goodsDetailResponse.getCode())) {
            throw new TradeBusinessException("The goods/product does not exist.");
        }

        // Check vendor code
        if(StringUtils.isEmpty(goodsDetailResponse.getVendorCode())
                || goodsDetailResponse.getVendorCode().equals(request.getVendorCode())) {
            throw new TradeBusinessException("The vendor code is invalid.");
        }

        // Save info of plan
        this.planRepository.save(Plan.builder()
                        .id(AppUtils.generateUUID(UUIDPrefixEnum.PLAN.getValue()))
                        .name(request.getName())
                        .vendorCode(request.getVendorCode())
                        .totalAmount(request.getTotalAmount())
                        .unitPrice(request.getUnitPrice())
                        .effectiveFromDate(request.getEffectiveFromDate())
                        .effectiveToDate(request.getEffectiveToDate())
                        .isTerminated(false)
                        .build());
    }

    /**
     * Update the info of plan
     *
     * @param request
     * @throws TradeBusinessException
     */
    @Override
    public void updatePlan(PlanRequest request) throws TradeBusinessException {

        // Check plan id parameter
        if (StringUtils.isEmpty(request.getPlanId())) {
            throw new TradeBusinessException("The planID is not empty.");
        }

        Plan plan = this.planRepository.getReferenceById(request.getPlanId());
        if (plan == null) {
            throw new TradeBusinessException("The plan does not exist.");
        }

        // Get item details from product module
        GoodsDetailResponse goodsDetailResponse = this.productClientService.getProductDetail(request.getProductCode());
        if (goodsDetailResponse == null || StringUtils.isEmpty(goodsDetailResponse.getCode())) {
            throw new TradeBusinessException("The goods/product does not exist.");
        }

        // Check vendor code
        if(StringUtils.isEmpty(goodsDetailResponse.getVendorCode())
                || goodsDetailResponse.getVendorCode().equals(request.getVendorCode())) {
            throw new TradeBusinessException("The vendor code is invalid.");
        }

        // Update info and save into the database
        this.updateInfoOfPlan(request, plan);
        this.planRepository.save(plan);
    }

    /**
     * Delete/terminate a plan
     *
     * @param planId
     * @throws TradeBusinessException
     */
    @Override
    public void terminatePlan(String planId) throws TradeBusinessException {

        // Check exist plan in database
        Plan plan = this.planRepository.getReferenceById(planId);
        if (plan == null) {
            throw new TradeBusinessException("The plan does not exist.");
        }

        // Terminate plan
        plan.setIsTerminated(true);
        this.planRepository.save(plan);
    }

    private void updateInfoOfPlan(PlanRequest request, Plan plan) {
        plan.setName(request.getName());
        plan.setTotalAmount(request.getTotalAmount());
        plan.setProductCode(request.getProductCode());
        plan.setEffectiveFromDate(request.getEffectiveFromDate());
        plan.setEffectiveToDate(request.getEffectiveToDate());
        plan.setUnitPrice(request.getUnitPrice());
    }
}

package com.omg.trade.service;

import com.omg.trade.exception.TradeBusinessException;
import com.omg.trade.model.request.GoodsPageRequest;
import com.omg.trade.model.request.GoodsToCartRequest;
import com.omg.trade.model.request.RemoveGoodsRequest;
import com.omg.trade.model.response.PageResultResponse;
import com.omg.trade.model.response.GoodsDetailResponse;

/**
 * @author nhcuong
 */
public interface ICartService {

    GoodsDetailResponse getProductDetailsByCode(String code) throws TradeBusinessException;

    void addGoodsToCart(GoodsToCartRequest request) throws TradeBusinessException;

    void removeGoodsFromCart(RemoveGoodsRequest request) throws TradeBusinessException;

    PageResultResponse<GoodsDetailResponse> getGoodsOfCart(GoodsPageRequest request) throws TradeBusinessException;
}

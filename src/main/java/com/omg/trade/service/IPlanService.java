package com.omg.trade.service;

import com.omg.trade.exception.TradeBusinessException;
import com.omg.trade.model.request.PlanRequest;

public interface IPlanService {

    void createPlan(PlanRequest request) throws TradeBusinessException;

    void updatePlan(PlanRequest request) throws TradeBusinessException;

    void terminatePlan(String planId) throws TradeBusinessException;
}

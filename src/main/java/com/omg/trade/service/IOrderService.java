package com.omg.trade.service;

import com.omg.trade.exception.TradeBusinessException;
import com.omg.trade.model.request.CancelOrderRequest;
import com.omg.trade.model.request.ConfirmOrderRequest;
import com.omg.trade.model.request.OrderRequest;

/**
 * @author cuongnh
 */
public interface IOrderService {

    void placeAndOrder(OrderRequest request) throws TradeBusinessException;

    void confirmOrder(ConfirmOrderRequest request) throws TradeBusinessException;

    void cancelOrder(CancelOrderRequest request) throws TradeBusinessException;
}

package com.omg.trade.service;

import com.omg.trade.model.response.PageResultResponse;
import com.omg.trade.model.response.GoodsDetailResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(name="PRODUCT")
public interface IProductClientService {

    @GetMapping("api/product/details/{code}")
    public GoodsDetailResponse getProductDetail(@PathVariable("code") String code);

    @GetMapping("api/product/page/by-codes")
    public PageResultResponse<GoodsDetailResponse> getProductPageByCodes(@RequestParam Integer pageIndex,
                                                                         @RequestParam Integer pageSize,
                                                                         @RequestParam List<String> productCodes);

    @GetMapping("api/product/by-codes")
    public List<GoodsDetailResponse> getProducstByCodes(@RequestParam List<String> productCodes);
}
